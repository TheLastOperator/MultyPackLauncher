#ifndef MODPACK_H
#define MODPACK_H

#include <QObject>
#include <QList>

#include "library.h"
#include "asset.h"
#include "mod.h"

#include "clientfile.h"

class Modpack : public QObject
{
    Q_OBJECT
public:
    explicit Modpack(QString manifestUrl, QObject *parent = 0);
    ~Modpack();

    QList<Library *> libraries();
    QList<Mod *> mods();
    QList<ClientFile *> configs();
    QList<ClientFile *> natives();
    QList<Asset *> assets();

    ClientFile *assetsIndex();
    ClientFile *client();
    ClientFile *options();
    ClientFile *servers();

    QList<ClientFile *> resourcepacks();


    QString args() const;

    QString mcVersion() const;

    QString mainClass() const;

private:
    QList<Library*> m_libraries;
    QList<Mod*> m_mods;
    QList<ClientFile*> m_configs;
    QList<ClientFile*> m_natives;
    QList<Asset*> m_assets;
    QList<ClientFile*> m_resourcepacks;
    ClientFile* m_assetsIndex;
    ClientFile* m_client;
    ClientFile* m_options;
    ClientFile* m_servers;
    QString m_args;
    QString m_mcVersion;
    QString m_mainClass;
};

#endif // MODPACK_H

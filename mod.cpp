#include "mod.h"

Mod::Mod(QString name, QString hash, int size, QString baseUrl, QObject *parent) : Downloadable(parent)
{
    m_name = name;
    m_hash = hash;
    m_size = size;
    m_baseUrl = baseUrl;
}

Mod::~Mod()
{

}

QString Mod::localPath()
{
    return "mods/" + m_name;
}

QString Mod::remotePath()
{
    return "http://"+m_baseUrl+"/" + m_name;
}

QString Mod::checksum()
{
    return m_hash;
}

int Mod::size()
{
    return m_size;
}

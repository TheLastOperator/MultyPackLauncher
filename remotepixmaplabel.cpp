#include "remotepixmaplabel.h"

#include <QPixmap>
#include <QUrl>
#include <QDebug>

void RemotePixmapLabel::setRemotePixmap(const QString& url)
{
    /* Here might be the work with the cached pixmaps */

    if (!m_networkManager) {
        /* Lazy initialization of QNetworkAccessManager;
         * only 1 instance is required;
         */
        m_networkManager = new QNetworkAccessManager(this);
        connect(m_networkManager, SIGNAL(finished(QNetworkReply*)),
                this, SLOT(pixmapReceived(QNetworkReply*)));
    }

    /* Send GET request to obtain the desired picture */
    m_networkManager->get(QNetworkRequest(QUrl(url)));
}

void RemotePixmapLabel::pixmapReceived(QNetworkReply* reply)
{
    if (QNetworkReply::NoError != reply->error()) {
        qDebug() << Q_FUNC_INFO << "pixmap receiving error" << reply->error();
        reply->deleteLater();
        return;
    }

    const QByteArray data(reply->readAll());
    if (!data.size())
        qDebug() << Q_FUNC_INFO << "received pixmap looks like nothing";

    QPixmap pixmap;
    pixmap.loadFromData(data);
    setPixmap(pixmap);

    reply->deleteLater();
}

void RemotePixmapLabel::mouseReleaseEvent(QMouseEvent *event)
{
    emit clicked();
}

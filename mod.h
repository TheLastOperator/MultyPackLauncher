#ifndef MOD_H
#define MOD_H

#include "downloadable.h"

class Mod : public Downloadable
{
    Q_OBJECT
public:
    explicit Mod(QString name, QString hash, int size, QString baseUrl, QObject *parent = 0);
    ~Mod();

    QString localPath();
    QString remotePath();
    QString checksum();
    int size();

private:
    QString m_name;
    QString m_hash;
    int m_size;
    QString m_baseUrl;
};

#endif // MOD_H

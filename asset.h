#ifndef ASSET_H
#define ASSET_H

#include "downloadable.h"

class Asset : public Downloadable
{
    Q_OBJECT
public:
    explicit Asset(QString name, QString hash, int size, QObject *parent = 0);
    ~Asset();

    QString localPath();
    QString remotePath();
    QString checksum();
    int size();

private:
    QString m_name;
    QString m_hash;
    int m_size;
};

#endif // ASSET_H

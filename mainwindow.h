#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "modpack.h"
#include "downloader.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void refreshUserLabel();

signals:
    void launchRequested();

protected:
    void mouseMoveEvent(QMouseEvent *);
    void mousePressEvent(QMouseEvent *);

private slots:
    void on_playPushButton_clicked();
    void on_labelSkin_clicked();

    void on_optionsButton_clicked();

private:
    Ui::MainWindow *ui;
    QPoint m_dragPosition;
    Downloader m_modUpdater;

};

#endif // MAINWINDOW_H

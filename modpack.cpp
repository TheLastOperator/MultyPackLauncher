#include "modpack.h"

#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QEventLoop>
#include <QUrl>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QStandardPaths>

#include <QDebug>

Modpack::Modpack(QString manifestUrl, QObject *parent) : QObject(parent)
{
    QNetworkAccessManager manager;

    QNetworkReply *reply;
    QEventLoop eventLoop;

    reply = manager.get(QNetworkRequest(QUrl(manifestUrl)));

    QObject::connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();

    QJsonObject manifestJsonObject;
    manifestJsonObject = QJsonDocument::fromJson(reply->readAll()).object();
    reply->deleteLater();

    QString assetsVersion = manifestJsonObject.value("assets").toString();
    QString assetsIndexUrl = "http://s3.amazonaws.com/Minecraft.Download/indexes/"+assetsVersion+".json";

    reply = manager.get(QNetworkRequest(QUrl(assetsIndexUrl)));

    QObject::connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
    eventLoop.exec();

    QJsonObject assetsJsonObject;
    assetsJsonObject = QJsonDocument::fromJson(reply->readAll()).object();
    reply->deleteLater();

    QJsonObject::ConstIterator it;
    for (it = assetsJsonObject.value("objects").toObject().constBegin(); it != assetsJsonObject.value("objects").toObject().constEnd(); ++it)
    {
        Asset *asset = new Asset("", (*it).toObject().value("hash").toString(), (*it).toObject().value("size").toInt(), this);
        m_assets.append(asset);
    }

    QJsonArray librariesJsonArray;
    librariesJsonArray = manifestJsonObject.value("libraries").toArray();

    for(QJsonValue jsonValue : librariesJsonArray) {
        Library *library = new Library(jsonValue.toObject().value("name").toString(), jsonValue.toObject().value("hash").toString(), 0, jsonValue.toObject().value("url").toString("https://libraries.minecraft.net"), this);
        m_libraries.append(library);
    }

    QJsonArray modsJsonArray;
    modsJsonArray = manifestJsonObject.value("mods").toObject().value("required").toArray();
    QString modsBaseUrl = manifestJsonObject.value("mods").toObject().value("baseUrl").toString();

    for(QJsonValue jsonValue : modsJsonArray) {
        Mod *mod = new Mod(jsonValue.toObject().value("name").toString(), jsonValue.toObject().value("hash").toString(), 0, jsonValue.toObject().value("url").toString(modsBaseUrl), this);
        m_mods.append(mod);
    }

    QJsonArray configsJsonArray;
    configsJsonArray = manifestJsonObject.value("config").toArray();

    for(QJsonValue jsonValue : configsJsonArray) {
        QString configName = jsonValue.toObject().value("name").toString();
        ClientFile *config = new ClientFile("config/" + configName, "http://launcher.galharim.fr/config/" + configName, jsonValue.toObject().value("hash").toString(), 0, this);
        m_configs.append(config);
    }

    QString nativesOS;
    #ifdef Q_OS_WIN
        nativesOS = "win";
    #endif
    #ifdef Q_OS_MAC
        nativesOS = "mac";
    #endif
    #ifdef Q_OS_LINUX
        nativesOS = "linux";
    #endif

    QJsonArray nativesJsonArray;
    nativesJsonArray = manifestJsonObject.value("natives").toObject().value(nativesOS).toArray();
    QString nativesBaseUrl = manifestJsonObject.value("natives").toObject().value("baseUrl").toString();

    for(QJsonValue jsonValue : nativesJsonArray) {
        ClientFile *native = new ClientFile("natives/" + jsonValue.toObject().value("name").toString(), jsonValue.toObject().value("url").toString(nativesBaseUrl) + "/" + nativesOS + "/" + jsonValue.toObject().value("name").toString(), jsonValue.toObject().value("hash").toString(), 0, this);
        m_natives.append(native);
    }

    QJsonArray resourcepacksJsonArray;
    resourcepacksJsonArray = manifestJsonObject.value("resourcepacks").toArray();

    for(QJsonValue jsonValue : resourcepacksJsonArray) {
        ClientFile *resourcepack = new ClientFile("resourcepacks/" + jsonValue.toObject().value("name").toString(), jsonValue.toObject().value("url").toString() + "/" + jsonValue.toObject().value("name").toString(), jsonValue.toObject().value("hash").toString(), 0, this);
        m_natives.append(resourcepack);
    }

    m_assetsIndex = new ClientFile("assets/indexes/" + assetsVersion + ".json", "https://s3.amazonaws.com/Minecraft.Download/indexes/" + assetsVersion + ".json", "", 0, this);
    m_client = new ClientFile("minecraft.jar", "https://s3.amazonaws.com/Minecraft.Download/versions/" + assetsVersion + "/" + assetsVersion + ".jar", "", 0, this);

    m_options = manifestJsonObject.contains("options") ? new ClientFile("options.txt", manifestJsonObject.value("options").toString(), "", 0, this) : nullptr;
    m_servers = manifestJsonObject.contains("servers") ? new ClientFile("servers.dat", manifestJsonObject.value("servers").toString(), "", 0, this) : nullptr;

    m_mcVersion = manifestJsonObject.value("id").toString();
    m_args = manifestJsonObject.value("minecraftArguments").toString();
    m_mainClass = manifestJsonObject.value("mainClass").toString();
}

Modpack::~Modpack()
{

}

QList<Library *> Modpack::libraries()
{
    return m_libraries;
}

QList<Mod *> Modpack::mods()
{
    return m_mods;
}

QList<ClientFile *> Modpack::configs()
{
    return m_configs;
}

QList<ClientFile *> Modpack::natives()
{
    return m_natives;
}

QList<Asset*> Modpack::assets()
{
    return m_assets;
}

ClientFile *Modpack::assetsIndex()
{
    return m_assetsIndex;
}

ClientFile *Modpack::client()
{
    return m_client;
}

ClientFile *Modpack::options()
{
    return m_options;
}

ClientFile *Modpack::servers()
{
    return m_servers;
}

QList<ClientFile *> Modpack::resourcepacks()
{
    return m_resourcepacks;
}

QString Modpack::args() const
{
    return m_args;
}

QString Modpack::mcVersion() const
{
    return m_mcVersion;
}

QString Modpack::mainClass() const
{
    return m_mainClass;
}

#ifndef REMOTE_PIXMAP_LABEL_H
#define REMOTE_PIXMAP_LABEL_H

#include <QObject>
#include <QLabel>
#include <QNetworkAccessManager>
#include <QNetworkReply>

class RemotePixmapLabel: public QLabel
{
    Q_OBJECT
public:
    RemotePixmapLabel(QWidget* parent = 0) :
        QLabel(parent), m_networkManager(0)
    {}
    RemotePixmapLabel(const QString& text, QWidget* parent = 0) :
        QLabel(text, parent), m_networkManager(0)
    {
    }
signals:
    void clicked();
public slots:
    void setRemotePixmap(const QString& /* url */);
private slots:
    void pixmapReceived(QNetworkReply*);
protected:
    void mouseReleaseEvent ( QMouseEvent * event ) ;
private:
    QNetworkAccessManager* m_networkManager;
};

#endif

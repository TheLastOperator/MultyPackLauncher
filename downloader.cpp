#include "downloader.h"

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QDir>
#include <QApplication>

#include <QDebug>

Downloader::Downloader(QObject *parent) : QObject(parent)
{
    m_currentIndex = 0;
}

Downloader::~Downloader()
{

}

bool Downloader::hasFinished()
{
    return !m_downloadQueue.isEmpty() ? m_downloadQueueIterator == m_downloadQueue.constEnd() && m_currentDownload->isFinished() : false;
}

void Downloader::addItem(Downloadable *item)
{
    m_downloadQueue.append(item);
}

void Downloader::addItems(QList<Downloadable *> &items)
{
    m_downloadQueue.append(items);
}

void Downloader::startUpdate()
{
    m_downloadQueueIterator = m_downloadQueue.constBegin();

    emit progressMaxUpdate(m_downloadQueue.size());
    emit progressUpdate(m_currentIndex);

    startNextDownload();
}

void Downloader::startNextDownload()
{
    if(m_downloadQueueIterator == m_downloadQueue.constEnd()) {
        emit finished();
        return;
    }
    qDebug() << "Download : " << (*m_downloadQueueIterator)->checksum();
    QString hash = (*m_downloadQueueIterator)->checksum();

    QDir dir((*m_downloadQueueIterator)->localPath());
    if (!dir.exists((*m_downloadQueueIterator)->localPath())) {
        dir.mkpath("..");
    }
    //qDebug() << (*m_downloadQueueIterator)->localPath();
    m_output.setFileName((*m_downloadQueueIterator)->localPath());

    QString localHash = "";

    if (m_output.open(QFile::ReadOnly)) {
        QCryptographicHash hash(QCryptographicHash::Sha1);
        if (hash.addData(&m_output)) {
            localHash = hash.result().toHex();
            qDebug() << "LocalHash : " << localHash;
        }
        m_output.close();
    }

    QApplication::instance()->processEvents();

    if((localHash != "") && ((hash == "") || (hash == localHash))) {
        m_downloadQueueIterator++;
        m_currentIndex++;
        emit progressUpdate(m_currentIndex);
        startNextDownload();
    }
    else {
        m_output.open(QIODevice::WriteOnly);
        m_currentDownload = m_manager.get(QNetworkRequest(QUrl((*m_downloadQueueIterator)->remotePath())));
        connect(m_currentDownload, &QNetworkReply::finished, this, &Downloader::downloadFinished);
        connect(m_currentDownload, &QNetworkReply::readyRead, this, &Downloader::downloadReadyRead);
    }
}

void Downloader::downloadFinished()
{
    m_output.close();
    m_currentDownload->deleteLater();
    startNextDownload();
}

void Downloader::downloadReadyRead()
{
    m_output.write(m_currentDownload->readAll());
}

#include "clientfile.h"

ClientFile::ClientFile(QString localPath, QString remotePath, QString checksum, int size, QObject *parent)
{
    m_localPath = localPath;
    m_remotePath = remotePath;
    m_checksum = checksum;
    m_size = size;
}

ClientFile::~ClientFile()
{

}

QString ClientFile::localPath()
{
    return m_localPath;
}

QString ClientFile::remotePath()
{
    return m_remotePath;
}

QString ClientFile::checksum()
{
    return m_checksum;
}

int ClientFile::size()
{
    return m_size;
}


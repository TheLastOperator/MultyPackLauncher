#ifndef DOWNLOADER_H
#define DOWNLOADER_H

#include <QFile>
#include <QObject>
#include <QUrl>
#include <QList>
#include <QNetworkAccessManager>

#include "downloadable.h"

class Downloader : public QObject
{
    Q_OBJECT
public:
    explicit Downloader(QObject *parent = 0);
    ~Downloader();

    bool hasFinished();

signals:
    void finished();
    void progressMaxUpdate(int);
    void progressUpdate(int);

public slots:
    void addItem(Downloadable *item);
    void addItems(QList<Downloadable*> &items);
    void startUpdate();

private slots:
    void startNextDownload();
    void downloadFinished();
    void downloadReadyRead();

private:
    QNetworkAccessManager m_manager;
    QList<Downloadable*> m_downloadQueue;
    QList<Downloadable*>::const_iterator m_downloadQueueIterator;
    QNetworkReply *m_currentDownload;
    QFile m_output;
    int m_currentIndex;
};

#endif // DOWNLOADER_H

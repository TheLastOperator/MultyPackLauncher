#include "asset.h"

Asset::Asset(QString name, QString hash, int size, QObject *parent) : Downloadable(parent)
{
    m_name = name;
    m_hash = hash;
    m_size = size;
}

Asset::~Asset()
{

}

QString Asset::localPath()
{
    return "assets/objects/" + m_hash.left(2) + "/" + m_hash;
}

QString Asset::remotePath()
{
    return "http://resources.download.minecraft.net/" + m_hash.left(2) + "/" + m_hash;
}

QString Asset::checksum()
{
    return m_hash;
}

int Asset::size()
{
    return m_size;
}


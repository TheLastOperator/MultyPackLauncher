#include "launcher.h"

#include <QApplication>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QMessageBox>
#include <QSettings>
#include <QUuid>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include <QDir>
#include <QStandardPaths>

#include <QStringList>

#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include <QDebug>

#include "downloadable.h"
#include "logindialog.h"


Launcher::Launcher(QObject *parent) : QObject(parent)
{
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));
    if (!dir.exists()) {
        dir.mkpath(".");
    }
    QDir::setCurrent(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));

    m_downloader = new Downloader;

    m_mcprocess = nullptr;

    m_updateDialog = new UpdateDialog(m_downloader);
    m_updateDialog->setWindowFlags(Qt::FramelessWindowHint);

    m_mainWindow = new MainWindow();
    m_mainWindow->setWindowFlags(Qt::FramelessWindowHint);
    connect(m_mainWindow, &MainWindow::launchRequested, this, &Launcher::startGame);
}

Launcher::~Launcher()
{
    delete m_mainWindow;
    delete m_updateDialog;
    delete m_modpack;
    delete m_downloader;
}

void Launcher::run()
{
    // Begin Update Launcher

    QEventLoop eventLoop;

    if(!QSslSocket::supportsSsl()) {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Fonctionnalité manquante");
        msgBox.setTextFormat(Qt::RichText);
        msgBox.setText("Le launcher a besoin de <a href='http://slproweb.com/products/Win32OpenSSL.html'>OpenSSL</a> pour fonctionner.");
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
        exit(0);
    }

    QNetworkRequest req( QUrl( QString("http://launcher.galharim.fr/version") ) );

    QNetworkReply *reply = m_networkAccessManager.get(req);

    connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));

    eventLoop.exec(); // blocks stack until "finished()" has been called

    // End Update Launcher


    if (reply->error() == QNetworkReply::NoError) {
        if(reply->readAll() == "2.0.4\n") {
            delete reply;
            normalCycle();
        }
        else {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Mise à jour disponnible");
            msgBox.setTextFormat(Qt::RichText);
            msgBox.setText("Une mise à jour du launcher est disponnible !\n"
                           "Veuillez la télécharger manuellement sur le site de galharim:\n"
                           "<a href='https://galharim.fr/launcher'>https://galharim.fr/launcher</a>");
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.exec();

            delete reply;
            exit(0);
        }
    }
    else {
        QMessageBox::critical(0, ("Echec de la vérification"),
                                      ("La vérification de la mise à jour du launcher a échouée pour la raison suivante:\n"+
                                          reply->errorString()+
                                          "\nImpossible de démarrer le launcher."));
        delete reply;
        exit(-1);
    }

}

void Launcher::loadModpack(QString url)
{
    QSettings settings;

    qDebug() << "Loading Modpack 1/2 ...";

    m_modpack = new Modpack(url);

    qDebug() << "Loading Modpack 2/2 ...";

    for(Downloadable *downloadable : m_modpack->libraries()) {
        m_downloader->addItem(downloadable);
    }

    for(Downloadable *downloadable : m_modpack->assets()) {
        m_downloader->addItem(downloadable);
    }

    for(Downloadable *downloadable : m_modpack->mods()) {
        m_downloader->addItem(downloadable);
    }

    for(Downloadable *downloadable : m_modpack->configs()) {
        m_downloader->addItem(downloadable);
    }

    for(Downloadable *downloadable : m_modpack->natives()) {
        m_downloader->addItem(downloadable);
    }

    for(Downloadable *downloadable : m_modpack->resourcepacks()) {
        m_downloader->addItem(downloadable);
    }

    m_downloader->addItem(m_modpack->assetsIndex());
    m_downloader->addItem(m_modpack->client());

    if(m_modpack->options() != nullptr) {
        m_downloader->addItem(m_modpack->options());
    }

    if(m_modpack->servers() != nullptr) {
        m_downloader->addItem(m_modpack->servers());
    }

    qDebug() << "Modpack loaded.";

    if(settings.value("packIsUpToDate", false).toBool()) {
        qDebug() << "Modpack up to date, showing main frame.";
        m_mainWindow->show();
    }
    else {
        qDebug() << "Modpack NOT up to date, updating modpack...";
        connect(m_downloader, SIGNAL(finished()), this, SLOT(onUpdateComplete()));
        m_updateDialog->show();
        m_downloader->startUpdate();
    }
}

void Launcher::updateMods()
{

}

void Launcher::startGame()
{
    QSettings settings;

    QDir modsDir(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation));

    if(modsDir.cd("./mods")) {
        foreach(QString file, modsDir.entryList(QDir::Files)) {

            bool keep = false;
            for(Mod *mod : m_modpack->mods()) {
                if(mod->localPath() == "mods/"+file) {
                    keep = true;
                }
            }
            if(!keep) {
                QFile::remove(QStandardPaths::writableLocation(QStandardPaths::AppDataLocation)+"/mods/"+file);
            }
        }
    }

    QMap<QString, QString> argsDictionary;
    argsDictionary.insert(QString("auth_player_name"), settings.value("playerName").toString());
    argsDictionary.insert(QString("version_name"), m_modpack->mcVersion());
    argsDictionary.insert(QString("game_directory"), QString("./"));
    argsDictionary.insert(QString("assets_root"), QString("assets"));
    argsDictionary.insert(QString("assets_index_name"), m_modpack->mcVersion());
    argsDictionary.insert(QString("auth_uuid"), settings.value("playerId").toString());
    argsDictionary.insert(QString("auth_access_token"), settings.value("accessToken").toString());
    argsDictionary.insert(QString("user_properties"), QString("{}"));

    QStringList argsList;

    argsList.append("-Xmx" + settings.value("javaXMX", 2048).toString() + "M");
    argsList.append("-Xms" + settings.value("javaXMS", 2048).toString() + "M");

    argsList.append("-Djava.library.path=" + QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/natives/");

    argsList.append("-cp");

    QString libraries;
    for(Library *library : m_modpack->libraries()) {
        libraries += QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/" + library->localPath() + ";";
    }
    libraries += QStandardPaths::writableLocation(QStandardPaths::AppDataLocation) + "/minecraft.jar";
    argsList.append(libraries);

    argsList.append(m_modpack->mainClass());

    QRegularExpression regex("\\$\\{([a-z_]+)\\}");

    QStringList mcArgs = m_modpack->args().split(" ");

    for(QString arg : mcArgs) {
        QRegularExpressionMatch match = regex.match(arg);
        if(match.hasMatch()) {
           argsList.append(argsDictionary.value(match.captured(1), ""));
        }
        else {
            argsList.append(arg);
        }
    }

    //qDebug() << argsList;
    delete m_mcprocess;
    m_mcprocess = new QProcess(this);
    connect(m_mcprocess, SIGNAL(finished(int)), m_mainWindow, SLOT(show()));
    m_mcprocess->start("java", argsList);
    m_mainWindow->hide();

}

void Launcher::onUpdateComplete()
{
    QSettings settings;
    settings.setValue("packIsUpToDate", true);
    m_mainWindow->show();
}

void Launcher::onTokenValidationResponse()
{
    QSettings settings;

    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());

    if(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() != 204) {
        qDebug() << "Invalid token from previous session. Refreshing...";
        refreshToken();
    }
    else {
        qDebug() << "Valid token from previous session. Continue...";

        loadModpack("http://launcher.galharim.fr/manifest-beta.json");
    }
}

void Launcher::onTokenRefreshResponse()
{
    QSettings settings;

    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());

    if(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 200) {
        qDebug() << "Refresh successfull";
        QJsonDocument jsonReply = QJsonDocument::fromJson(reply->readAll());
        QJsonObject jsonObject = jsonReply.object();

        QString accessToken = jsonObject.value("accessToken").toString();

        QSettings settings;
        settings.setValue("accessToken", accessToken);
        validateToken();
    }
    else {
        qDebug() << "Invalid token from previous session. Asking for auth.";
        LoginDialog d;
        if(d.exec()) {
            m_mainWindow->refreshUserLabel();
            validateToken();
        }
        else {
            exit(0);
        }
    }
}

void Launcher::normalCycle()
{
    QSettings settings;

    if(!settings.contains("UUID")) {
        settings.setValue("UUID", QUuid::createUuid().toString());
    }

    qDebug() << "Launcher instance spawned with UUID: " << settings.value("UUID").toString();

    validateToken();
}

void Launcher::validateToken()
{
    QSettings settings;

    if(settings.contains("accessToken")) {
        qDebug() << "Token loaded from previous session. Validating...";

        QJsonObject jsonPayload;

        jsonPayload.insert("accessToken", settings.value("accessToken").toString());
        jsonPayload.insert("clientToken", settings.value("UUID").toString());

        QUrl url("https://authserver.mojang.com/validate");
        QNetworkRequest request(url);

        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

        QNetworkReply *reply = m_networkAccessManager.post(request, QJsonDocument(jsonPayload).toJson());

        QObject::connect(reply, SIGNAL(finished()), this, SLOT(onTokenValidationResponse()));
    }
    else {
        qDebug() << "No Token loaded from previous session. Asking for auth.";
        LoginDialog d;
        if(d.exec()) {
            m_mainWindow->refreshUserLabel();
            validateToken();
        }
        else {
            exit(0);
        }
    }
}

void Launcher::refreshToken()
{
    QSettings settings;

    if(settings.contains("accessToken")) {
        qDebug() << "Token loaded from previous session. Refreshing...";

        QJsonObject jsonPayload;

        jsonPayload.insert("accessToken", settings.value("accessToken").toString());
        jsonPayload.insert("clientToken", settings.value("UUID").toString());

        QUrl url("https://authserver.mojang.com/refresh");
        QNetworkRequest request(url);

        request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

        QNetworkReply *reply = m_networkAccessManager.post(request, QJsonDocument(jsonPayload).toJson());

        QObject::connect(reply, SIGNAL(finished()), this, SLOT(onTokenRefreshResponse()));
    }
    else {
        qDebug() << "No Token loaded from previous session. Asking for auth.";
        LoginDialog d;
        if(d.exec()) {
            m_mainWindow->refreshUserLabel();
            validateToken();
        }
        else {
            exit(0);
        }
    }
}

#ifndef LIBRARY_H
#define LIBRARY_H

#include "downloadable.h"

class Library : public Downloadable
{
    Q_OBJECT
public:
    explicit Library(QString name, QString hash, int size, QString baseUrl, QObject *parent = 0);
    ~Library();

    QString localPath();
    QString remotePath();
    QString checksum();
    int size();

private:
    QString m_name;
    QString m_hash;
    int m_size;
    QString m_baseUrl;
};

#endif // LIBRARY_H

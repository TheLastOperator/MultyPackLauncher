#include "logindialog.h"
#include "ui_logindialog.h"

#include <QSettings>
#include <QMessageBox>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::on_connectPushButton_clicked()
{
    QJsonObject jsonPayload;
    QJsonObject jsonAgent;

    QSettings settings;

    jsonAgent.insert("name", "Minecraft");
    jsonAgent.insert("version", 1);

    jsonPayload.insert("agent", jsonAgent);

    jsonPayload.insert("username", ui->loginLineEdit->text());
    jsonPayload.insert("password", ui->passwordLineEdit->text());
    jsonPayload.insert("clientToken", settings.value("UUID").toString());

    QNetworkAccessManager *manager = new QNetworkAccessManager(this);

    QUrl url("https://authserver.mojang.com/authenticate");
    QNetworkRequest request(url);

    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    QObject::connect(manager, SIGNAL(finished(QNetworkReply *)), this, SLOT(authReplyFinished(QNetworkReply *)));

    manager->post(request, QJsonDocument(jsonPayload).toJson());

    ui->connectPushButton->setDisabled(true);

}

void LoginDialog::authReplyFinished(QNetworkReply *reply)
{
    ui->connectPushButton->setDisabled(false);
    if(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 200) {
        QJsonDocument jsonReply = QJsonDocument::fromJson(reply->readAll());
        QJsonObject jsonObject = jsonReply.object();
        QJsonObject jsonProfile = jsonObject.value("selectedProfile").toObject();

        QString accessToken = jsonObject.value("accessToken").toString();
        QString playerId = jsonProfile.value("id").toString();
        QString playerName = jsonProfile.value("name").toString();

        QSettings settings;
        settings.setValue("accessToken", accessToken);
        settings.setValue("playerId", playerId);
        settings.setValue("playerName", playerName);
        accept();
    }
    else {
        QString serverReply = reply->readAll();
        QMessageBox messageBox;
        messageBox.setWindowTitle(tr("Echec de l'authentification"));
        messageBox.setText(tr("Impossible de se connecter\n") +
                           QJsonDocument::fromJson(serverReply.toUtf8()).object().value("errorMessage").toString());
        messageBox.setDetailedText("Server code: " + reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toString() +
                                   "\nServer reply :\n" + serverReply +
                                   "\nNetwork error:" + reply->errorString());
        messageBox.setIcon(QMessageBox::Warning);
        messageBox.exec();
    }
    reply->deleteLater();
}

void LoginDialog::on_cancelPushButton_clicked()
{
    reject();
}

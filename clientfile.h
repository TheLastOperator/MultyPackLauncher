#ifndef CLIENTFILE_H
#define CLIENTFILE_H

#include "downloadable.h"

class ClientFile : public Downloadable
{
    Q_OBJECT
public:
    explicit ClientFile(QString localPath, QString remotePath, QString checksum, int size, QObject *parent = 0);
    ~ClientFile();

    QString localPath();
    QString remotePath();
    QString checksum();
    int size();

private:
    QString m_localPath;
    QString m_remotePath;
    QString m_checksum;
    int m_size;
};

#endif // CLIENTFILE_H

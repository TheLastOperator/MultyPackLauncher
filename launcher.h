#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <QObject>
#include <QProcess>

#include "mainwindow.h"
#include "updatedialog.h"
#include "downloader.h"
#include "modpack.h"

class Launcher : public QObject
{
    Q_OBJECT
public:
    explicit Launcher(QObject *parent = 0);
    ~Launcher();

    void run();

signals:

public slots:
    void loadModpack(QString url);
    void updateMods();
    void startGame();

private slots:
    void onUpdateComplete();
    void onTokenValidationResponse();
    void onTokenRefreshResponse();

private:
    MainWindow *m_mainWindow;
    UpdateDialog *m_updateDialog;
    Downloader *m_downloader;
    Modpack *m_modpack;
    QProcess *m_mcprocess;

    QNetworkAccessManager m_networkAccessManager;

    void normalCycle();
    void validateToken();
    void refreshToken();
};

#endif // LAUNCHER_H

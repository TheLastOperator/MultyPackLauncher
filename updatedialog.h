#ifndef UPDATEDIALOG_H
#define UPDATEDIALOG_H

#include <QDialog>

#include "downloader.h"

namespace Ui {
class UpdateDialog;
}

class UpdateDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UpdateDialog(Downloader *downloader, QWidget *parent = 0);
    ~UpdateDialog();

protected:
    void mouseMoveEvent(QMouseEvent *);
    void mousePressEvent(QMouseEvent *);

private slots:
    void on_cancelPushButton_clicked();

private:
    Ui::UpdateDialog *ui;
    QPoint m_dragPosition;
};

#endif // UPDATEDIALOG_H

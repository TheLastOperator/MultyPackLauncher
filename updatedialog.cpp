#include "updatedialog.h"
#include "ui_updatedialog.h"

#include <QMouseEvent>

#include "modpack.h"

UpdateDialog::UpdateDialog(Downloader *downloader, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdateDialog)
{
    ui->setupUi(this);

    connect(downloader, SIGNAL(progressUpdate(int)), ui->progressBar, SLOT(setValue(int)));
    connect(downloader, SIGNAL(progressMaxUpdate(int)), ui->progressBar, SLOT(setMaximum(int)));
    connect(downloader, SIGNAL(finished()), this, SLOT(reject()));

}

UpdateDialog::~UpdateDialog()
{
    delete ui;
}


void UpdateDialog::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton) {
        move(event->globalPos() - m_dragPosition);
        event->accept();
    }
}
void UpdateDialog::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        m_dragPosition = event->globalPos() - frameGeometry().topLeft();
        event->accept();
    }
}

void UpdateDialog::on_cancelPushButton_clicked()
{
    exit(0);
}

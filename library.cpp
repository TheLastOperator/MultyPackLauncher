#include "library.h"

#include <QStringList>

#include <QDebug>

Library::Library(QString name, QString hash, int size, QString baseUrl, QObject *parent) : Downloadable(parent)
{
    m_name = name;
    m_hash = hash;
    m_size = size;
    m_baseUrl = baseUrl;
}

Library::~Library()
{

}

QString Library::localPath()
{
    QString path = m_name.split(":").at(0);
    return "libraries/" + path.replace(".", "/") + "/" + m_name.split(":").at(1) + "/" + m_name.split(":").at(2) + "/" + m_name.split(":").at(1) + "-" + m_name.split(":").at(2) + ".jar";
}

QString Library::remotePath()
{
    QString path = m_name.split(":").at(0);
    return m_baseUrl + "/" + path.replace(".", "/") + "/" + m_name.split(":").at(1) + "/" + m_name.split(":").at(2) + "/" + m_name.split(":").at(1) + "-" + m_name.split(":").at(2) + ".jar";
}

QString Library::checksum()
{
    return m_hash;
}

int Library::size()
{
    return m_size;
}

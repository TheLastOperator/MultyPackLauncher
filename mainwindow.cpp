#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QMouseEvent>
#include <QSettings>

#include "logindialog.h"
#include "optionsdialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->closeButton, &QPushButton::clicked, QApplication::instance(), &QApplication::exit);

    connect(&m_modUpdater, SIGNAL(progressUpdate(int)), ui->progressBar, SLOT(setValue(int)));
    connect(&m_modUpdater, SIGNAL(progressMaxUpdate(int)), ui->progressBar, SLOT(setMaximum(int)));

    connect(&m_modUpdater, SIGNAL(progressUpdate(int)), ui->progressBar_2, SLOT(setValue(int)));
    connect(&m_modUpdater, SIGNAL(progressMaxUpdate(int)), ui->progressBar_2, SLOT(setMaximum(int)));
    /*
    for(Downloadable *downloadable : m_modpack->mods()) {
        m_modUpdater.addItem(downloadable);
    }*/
    QSettings settings;
    if(settings.contains("playerName")) {
        ui->labelSkin->setRemotePixmap("https://minotar.net/helm/" + settings.value("playerName").toString() + "/24");
        ui->labelNickname->setText(settings.value("playerName").toString());
    }
    else {
        ui->labelSkin->setRemotePixmap("https://minotar.net/helm/char/24");
        ui->labelNickname->setText("Player");
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::refreshUserLabel()
{
    QSettings settings;
    ui->labelSkin->setRemotePixmap("https://minotar.net/helm/" + settings.value("playerName").toString() + "/24");
    ui->labelNickname->setText(settings.value("playerName").toString());
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton) {
        move(event->globalPos() - m_dragPosition);
        event->accept();
    }
}
void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton) {
        m_dragPosition = event->globalPos() - frameGeometry().topLeft();
        event->accept();
    }
}

void MainWindow::on_playPushButton_clicked()
{
    //ui->playPushButton->setDisabled(true);
    emit launchRequested();
}

void MainWindow::on_labelSkin_clicked()
{
    LoginDialog d;
    d.exec();

    refreshUserLabel();
}

void MainWindow::on_optionsButton_clicked()
{
    OptionsDialog dialog;
    dialog.exec();
}

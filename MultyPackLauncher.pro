#-------------------------------------------------
#
# Project created by QtCreator 2015-08-29T17:12:36
#
#-------------------------------------------------

QT       += core gui network webkit webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MultyPackLauncher
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp \
        mainwindow.cpp \
    aerobutton.cpp \
    updatedialog.cpp \
    downloadable.cpp \
    asset.cpp \
    mod.cpp \
    modpack.cpp \
    downloader.cpp \
    library.cpp \
    remotepixmaplabel.cpp \
    logindialog.cpp \
    launcher.cpp \
    optionsdialog.cpp \
    clientfile.cpp

HEADERS  += mainwindow.h \
    aerobutton.h \
    updatedialog.h \
    downloadable.h \
    asset.h \
    mod.h \
    modpack.h \
    downloader.h \
    library.h \
    remotepixmaplabel.h \
    logindialog.h \
    launcher.h \
    optionsdialog.h \
    clientfile.h

FORMS    += mainwindow.ui \
    updatedialog.ui \
    logindialog.ui \
    optionsdialog.ui

RC_FILE = MultyPackLauncher.rc

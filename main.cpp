#include <QApplication>

#include "launcher.h"

int main(int argc, char *argv[])
{
    QApplication::setApplicationName("Galharim Launcher");
    QApplication::setOrganizationName("Galharim");
    QApplication a(argc, argv);

    Launcher l;

    l.run();

    return a.exec();
}

#include "optionsdialog.h"
#include "ui_optionsdialog.h"

#include <QSettings>
#include <QMessageBox>
#include <QProcess>

OptionsDialog::OptionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OptionsDialog)
{
    ui->setupUi(this);
    QSettings settings;
    ui->javaXMSspinBox->setValue(settings.value("javaXMS", 2048).toInt());
    ui->javaXMXspinBox->setValue(settings.value("javaXMX", 2048).toInt());
}

OptionsDialog::~OptionsDialog()
{
    delete ui;
}

void OptionsDialog::on_forceUpdatePushButton_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Forcer la mise à jour du modpack", "Le launcher vas redémarrer et syncroniser tout les fichiers\n"
                                                                          "Voullez vous vraiment forcer la mise à jour ?",
                                QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        QSettings settings;
        settings.setValue("packIsUpToDate", false);
        qApp->quit();
        QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
    }
}

void OptionsDialog::on_disconnectPushButton_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Déconnecter l'utilisateur", "Le launcher vas supprimer les informations de connection et redémarrer.\n"
                                                                      "Voullez vous vraiment déconnecter l'utilisateur ?",
                            QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        QSettings settings;
        settings.remove("accessToken");
        settings.remove("playerId");
        settings.remove("playerName");
        qApp->quit();
        QProcess::startDetached(qApp->arguments()[0], qApp->arguments());
    }
}

void OptionsDialog::on_buttonBox_accepted()
{
    QSettings settings;
    settings.setValue("javaXMS", ui->javaXMSspinBox->value());
    settings.setValue("javaXMX", ui->javaXMXspinBox->value());
}

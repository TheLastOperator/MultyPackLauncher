#ifndef DOWNLOADABLE_H
#define DOWNLOADABLE_H

#include <QObject>

class Downloadable : public QObject
{
    Q_OBJECT
public:
    explicit Downloadable(QObject *parent = 0);
    ~Downloadable();

    virtual QString localPath() = 0;
    virtual QString remotePath() = 0;
    virtual QString checksum() = 0;
    virtual int size() = 0;

signals:

public slots:
};

#endif // DOWNLOADABLE_H
